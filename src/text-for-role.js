const textForRole = (roles, textLines) => {
    let arrText = textLines.split("\n").map((t, i) => `${i+1}) ${t}`);
    let newText = "";
    for (let r = 0; r < roles.length; r++) {
        newText += roles[r] + ":\n";
        for (let j = 0; j < arrText.length; j++) {
            if (arrText[j].includes(roles[r] + ":")) {
                newText += arrText[j].replace(`${roles[r]}:`, "") + "\n";
            }
        }
        newText += "\n";

    }

    return newText
}

module.exports = textForRole;

